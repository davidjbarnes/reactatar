import React from 'react';
import PropTypes from 'prop-types';

const md5 = require('md5');

class Reactatar extends React.Component {
    constructor(props){
        super(props)
    }

    //https://twitter.com/(userName)/profile_image?size=original
    //http://graph.facebook.com/67563683055/picture?type=large

    render() {
        const {email, size, defaultImage, rating, rounded} = this.props;
        const hash = md5(email);
        const url = "https://www.gravatar.com/avatar/".concat(hash, "?s=", size, "&d=", defaultImage, "&r=", rating);

        const style = rounded ? {borderRadius:"50%"} : {};

        return(<img src={url} style={style}/>);
    }
}

Reactatar.propTypes = {
    email: PropTypes.string,
    size: PropTypes.number,
    defaultImage: PropTypes.oneOf(['mp','identicon','monsterid','wavatar','retro','robohash','blank']),
    rating: PropTypes.oneOf(['g','pg','r','x']),
    rounded: PropTypes.bool,
};

Reactatar.defaultProps = {
    email: '',
    size: 80,
    defaultImage: 'mp',
    rating: 'r',
    rounded: false,
};

export default Reactatar;
