# Reactatar

A simple Gravatar wrapper for React

## Getting Started

Install from npm:

```
$ npm i reactatar
```

Or get the source code here:

```
$ git clone git clone https://davidjbarnes@bitbucket.org/davidjbarnes/reactatar.git
```

## Usage

```
import Reactatar from 'Reactatar';

<Reactatar email={"nullableint@gmail.com"} size={100} defaultImage={"mp"} rounded />
```

## Props

- email: String
- size: Number (default: 80)
- defaultImage: Enum['mp','identicon','monsterid','wavatar','retro','robohash','blank'] (default: mp)
- rating: Enum['g','pg','r','x'] (default: r)
- rounded: Boolean (default: false)

## Contributing

Email me: NullableInt@gmail.com

## Authors

- **David J Barnes** - _NullableInt@gmail.com_

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
